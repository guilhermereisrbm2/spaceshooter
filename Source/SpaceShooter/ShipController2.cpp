// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceShooter.h"
#include "ShipController2.h"


// Sets default values
AShipController2::AShipController2()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShipController2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShipController2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShipController2::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

